package devops;
import javax.servlet.*;
import java.sql.*;

public class MyListener implements ServletContextListener{

	public void contextInitialized(ServletContextEvent arg0) {
	try{
	        Connection con=null;	
		Class.forName("com.mysql.jdbc.Driver");
		con= DriverManager.getConnection("jdbc:mysql://localhost:3306/vehiclemanagement", "root", "devops");
				
		
		PreparedStatement ps4=con.prepareStatement("CREATE TABLE PAYREGISTER(ID INT,USERNAME VARCHAR(4000), USERPASS VARCHAR(4000), BRANCH VARCHAR(4000),DATEOFJOINING VARCHAR(4000), DATEOFBIRTH VARCHAR(4000), SALARY VARCHAR(4000),CONSTRAINT PAYREGISTER_PK PRIMARY KEY AUTO_INCREMENT (ID))");
		ps4.executeUpdate();		
			
		
		ps4= con.prepareStatement("CREATE TABLE  TINSTALL(ID INT,TMODEL VARCHAR(4000),TNO VARCHAR(4000),INSURANCE VARCHAR(4000),INAME VARCHAR(4000), OWNER VARCHAR(4000),TFROM VARCHAR(4000), TTO VARCHAR(4000), IDATE DATE, MOBILE VARCHAR(100),STATUS VARCHAR(4000),CONSTRAINT TINSTALL_PK PRIMARY KEY AUTO_INCREMENT (ID))");
		ps4.executeUpdate();
		
		ps4= con.prepareStatement("CREATE TABLE  QUIZCONTACT(NAME VARCHAR(4000),EMAIL VARCHAR(4000),PHONE INT NOT NULL, MESSAGE VARCHAR(4000))");
		ps4.executeUpdate();
                con.close();
	        	
	}
		
	    catch(Exception e){
	    	e.printStackTrace();
	    	
	    }
	    }
	    
	    public void contextDestroyed(ServletContextEvent arg0) {
		System.out.println("project undeployed");
		
	}
}
