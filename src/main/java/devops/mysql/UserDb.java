package devops.mysql;

import java.util.ArrayList;
import devops.model.User;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class UserDb {
	
	public static String[][] users;
	public static List<User>  usersData;
	
	public static Map<String, User> userProfile=new HashMap<String,User>();
	
	public UserDb(){
		
		//Scope is to get this data from web.xml and database.
		User u1=new User("group5","group5");
		
		userProfile.put(u1.getUserName(), u1);
	}
}
