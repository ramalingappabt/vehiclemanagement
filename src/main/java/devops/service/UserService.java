package devops.service;
import devops.mysql.UserDb;
import devops.model.User;

import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class UserService {
	UserDb userDb;
	public UserService(){
		userDb=new UserDb();
	}
	
	public boolean isAuthorized(User signup){
		boolean isValidUser=false;
		
		if(signup!=null && signup.getUserName()!=null && signup.getPassword()!=null){
			 Set entrySet = userDb.userProfile.entrySet();
			 Iterator it = entrySet.iterator();
			 
			 while(it.hasNext()){
				 Map.Entry u = (Map.Entry)it.next();
					//System.out.println(u.getKey()+"\t"+u.getValue());
				 if(u.getKey().equals(signup.getUserName())){
					 isValidUser=true;
				 }
			 }	
		}		
		
		return isValidUser;
	}
}
