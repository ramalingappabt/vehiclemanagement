package devops.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import devops.model.User;
import devops.service.UserService;

//import devops.ilp1.model.User;
//import devops.ilp1.service.UserService;

public class UserLoginController extends HttpServlet {
	
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		String username=request.getParameter("username");
		String password=request.getParameter("userpass");
		
		if (username.isEmpty() || password.isEmpty()) {
			  RequestDispatcher rd = request.getRequestDispatcher("test.jsp");
			   out.println("<font color=red>Please fill all the fields</font>");
			   rd.include(request, response);
			  } else {
				
				    User signUp=new User("group5","group5");

					PrintWriter pout= response.getWriter();
					if (new UserService().isAuthorized(signUp)) {
						pout.write("Login successfull...");
						RequestDispatcher rd = request.getRequestDispatcher("/admin.jsp");
						rd.forward(request,response);
						return;
					}
					out.write("Login fail...");
					 
					RequestDispatcher rd = request.getRequestDispatcher("/test.jsp");
					rd.forward(request,response);
					
					
			  }
	}
}

