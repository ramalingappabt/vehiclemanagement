package devops;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxBinary;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import org.openqa.selenium.firefox.FirefoxOptions;

import devops.IntegrationTest;

import org.junit.*;
import static org.junit.Assert.*;

import java.io.File;

import org.junit.experimental.categories.Category;

@Category(IntegrationTest.class)
public class LoginFunctionalTest {

	static WebDriver driver;

	@BeforeClass
	public static void setup() {
	//	driver = new ChromeDriver();
		// new FirefoxDriver();
				FirefoxBinary firefoxBinary = new FirefoxBinary();
        firefoxBinary.addCommandLineOptions("--headless");
        System.setProperty("webdriver.gecko.driver", "/usr/local/bin/geckodriver");
        FirefoxOptions firefoxOptions = new FirefoxOptions();
        firefoxOptions.setBinary(firefoxBinary);
        
        driver = new FirefoxDriver(firefoxOptions);
	}

	@AfterClass
	public static void cleanUp() {
		driver.quit();
	}

	@Test
	public void loginSuccess() {
        driver.get("http://localhost:6080/VehicleManagement/test.jsp");


        WebElement username = driver.findElement(By.name("username"));
        WebElement pass = driver.findElement(By.name("userpass"));
        username.sendKeys("group5");
        pass.sendKeys("group5");
        WebElement input = driver.findElement(By.xpath("//input[@value='Login']"));
        input.click();
        assertTrue(driver.getPageSource().contains("Create New Manager"));
	}
	
	@Test
	public void loginFail() {
	        driver.get("http://localhost:6080/VehicleManagement/test.jsp");
	        WebElement username = driver.findElement(By.name("username"));
	        WebElement pass = driver.findElement(By.name("userpass"));
	        username.sendKeys("group5");
	        pass.sendKeys("group6");
	        WebElement input = driver.findElement(By.xpath("//input[@value='Login']"));
	        input.click();
	        assertTrue(driver.getPageSource().contains("Sorry! Username or Password Error"));
	}
}
